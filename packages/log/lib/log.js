export function make_logger (scope) {
    function log (message, data) {
        console.log (`${ scope }: ${ message }`, data)
    }
    log.narrow = (subscope) => {
        return make_logger (`${ scope } / ${ subscope }`)
    }

    return log
}
