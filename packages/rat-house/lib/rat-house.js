import * as configuration from "@rats/configuration"
import { make_logger } from "@rats/log"

const logger = make_logger ("rat house")
await main ()

function delay (time) {
  return new Promise ((resolve) => {
    setTimeout (resolve, time)
  })
}

async function main () {
  const log = logger.narrow ("main")
  const { frequency } = await configuration.load ("./configuration.json")
  log ("Configuration loaded")
  await dance (frequency)
}


async function dance (frequency) {
  const log = logger.narrow ("dance")
  log ("Let's dance!", { frequency })


  let done = 0
  while (done < 5) {
    done += 1
    log.narrow (done) ("Rats are dancing in the house 🐀")
    await delay (1000 / frequency)
  }

  log ("We are done dancing 🌛")
}
