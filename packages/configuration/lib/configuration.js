import { promises as fs } from "fs"
import { make_logger } from "@rats/log"

const logger = make_logger ("configuration")

export async function load(path) {
    const log = logger.narrow ("loading")
    log (`Reading ${ path }`)
    const json = await fs.readFile (path, "utf-8")
    const data = JSON.parse (json)
    return data
}
